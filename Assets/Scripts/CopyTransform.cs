﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CopyTransform : MonoBehaviour {

	public GameObject fromPreference = null;
	public GameObject particlePrefab = null;
	int interval = 180;
	int charaMax = 4;
	//GameObject[] charaArray = null;
	GameObject[] positionArray = null;
	int positionArrayCur = 0;
	Vector3 explosePosition = Vector3.zero;
	float exploseTime = 0.0f;

	// Use this for initialization
	void Start () {
		//Debug.Log("CopyTransform Start!!");
		// 分身分のインスタンス
		//charaArray = new GameObject[charaMax];
		//for (int i = 0; i < charaArray.Length; i++) {
		//    GameObject obj = Instantiate<GameObject>(fromPreference);
		//    obj.SetActive(false);
		//    obj.transform.parent = this.transform;
		//}
		// 座標のみ
		positionArray = new GameObject[interval * charaMax];
		for (int i = 0; i < positionArray.Length; i++) {
			positionArray[i] = null;
		}
	}

	// Update is called once per frame
	void Update () {
		if (true) {
			GameObject obj = Instantiate<GameObject>(fromPreference);
			obj.SetActive(false);
			obj.transform.parent = this.transform;
			ImprovePosition (obj);
			CustomAnimatorDriver cad = obj.GetComponent<CustomAnimatorDriver>();
			if (cad) {
				Object.Destroy(cad);
			}
			// 最大数を超えると削除
			if (positionArray[positionArrayCur]) {
				Object.Destroy(positionArray[positionArrayCur]);
			}
			positionArray[positionArrayCur] = obj;
			positionArrayCur++;
			positionArrayCur %= positionArray.Length;
		}

		// 表示と非表示を切り替える
		for (int i = 0; i < positionArray.Length; i++) {
			GameObject obj = positionArray[(positionArray.Length - 1 - i + positionArrayCur) % positionArray.Length];
			if (obj) {
				obj.SetActive(i % interval == interval - 1);
				//obj.SetActive(i % interval == 0);
				if (exploseTime > 0.0f) {
					Vector3 p = obj.transform.position;
					Vector3 n = Vector3.Normalize(obj.transform.position - explosePosition);
					n.y = 0.0f;
					p = n * exploseTime * 0.5f;
					obj.transform.position = p;
				}
			}
		}

		// クリックした場所を中心に遠ざける
		exploseTime -= Mathf.Max(0.0f, exploseTime - Time.deltaTime);
	}

	void ImprovePosition (GameObject obj) {
		// 座標をカメラの位置に矯正
		if (obj) {
			//Vector3 p = fromPreference.transform.position;
			//p.x = Mathf.Sin(3.0f * Time.time);
			// カメラの位置
			Vector3 p = Camera.main.transform.position;
			p.y = 0.0f;
			// 腰の位置を検出
			GameObject hip = null;
			foreach (Transform child1 in obj.transform) {
				child1.gameObject.SetActive (true);
				if (child1.gameObject.name.CompareTo("Robot_References") == 0) {
					foreach (Transform child2 in child1.transform) {
						if (child2.gameObject.name.CompareTo("Robot_Reference") == 0) {
							foreach (Transform child3 in child2.transform) {
								//Debug.Log ("name: " + child2.gameObject.name);
								if (child3.gameObject.name.CompareTo("Robot_Hips") == 0) {
									hip = child3.gameObject;
									break;
								}
							}
						}
					}
				}
			}

			if (hip) {
				//Debug.Log ("obj: " + obj.transform.position.x + "," + obj.transform.position.z);
				//Debug.Log ("hip: " + hip);
				p.x -= obj.transform.position.x + hip.transform.position.x;
				p.z -= obj.transform.position.z + hip.transform.position.z;
				//p.x -= obj.transform.position.x;
				//p.z -= obj.transform.position.z;
				//Debug.Log ("hip: " + p.x + "," + p.z);

				obj.transform.position = p;

				// 腰位置をゼロに
				//p = hip.transform.position;
				//p.x = 0.0f;
				//p.z = 0.0f;
				//hip.transform.position = p;
			}
		}
	}

	public void DoExplose() {
		//Debug.Log("DoExplose!!");
		Vector3 p = Camera.main.transform.position;
		p.y = 0.0f;
		explosePosition = p;
		exploseTime = 1.0f;
	}

	public void DoAllDelete() {
		for (int i = 0; i < positionArray.Length; i++) {
			GameObject obj = positionArray[i];
			if (obj) {
				if (obj.activeSelf && particlePrefab) {
					GameObject psObj = (GameObject)Instantiate (particlePrefab, obj.transform.position, obj.transform.rotation);
				}
				Object.Destroy(obj);
				positionArray[i] = null;
			}
		}
	}
}
