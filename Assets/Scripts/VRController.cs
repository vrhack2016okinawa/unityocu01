﻿using UnityEngine;
using System.Collections;

public class VRController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var trackedController = gameObject.GetComponent<SteamVR_TrackedController>();
		if (trackedController == null) {
			trackedController = gameObject.AddComponent<SteamVR_TrackedController>();
		}

		trackedController.TriggerClicked += new ClickedEventHandler(DoClick);
		trackedController.TriggerUnclicked += new ClickedEventHandler(DoUnclick);
	}

	// Update is called once per frame
	void Update () {
	}

	void DoClick(object sender, ClickedEventArgs e) {
		//Debug.Log("click!!");
		GameObject gameObj = GameObject.Find("CopyTransform");
		if (gameObj) {
			CopyTransform ct = gameObj.GetComponent<CopyTransform>();
			ct.DoAllDelete ();
		}
	}

	void DoUnclick(object sender, ClickedEventArgs e) {
		//Debug.Log("unclick!!");
	}
}
